<?php

namespace Marketing\ModelBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('MarketingModelBundle:Default:index.html.twig');
    }
}
