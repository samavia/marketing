<?php

namespace Marketing\ModelBundle\Controller;

use Marketing\ModelBundle\Entity\Adcode;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;


/**
 * Adcode controller.
 *
 */
class AdcodeController extends Controller
{
    /**
     * Lists all adcode entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $adcodes = $em->getRepository('MarketingModelBundle:Adcode')->findAll();

        return $this->render('adcode/index.html.twig', array(
            'adcodes' => $adcodes,
        ));
    }

    /**
     * Finds and displays a adcode entity.
     *
     */
    public function showAction(Adcode $adcode)
    {

        return $this->render('adcode/show.html.twig', array(
            'adcode' => $adcode,
        ));
    }
}
