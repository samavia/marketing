<?php

namespace Marketing\ModelBundle\Entity;

/**
 * Channelgroup
 */
class Channelgroup
{
    /**
     * @var integer
     */
    private $channelgroupid;

    /**
     * @var string
     */
    private $name;


    /**
     * Set channelgroupid
     *
     * @param integer $channelgroupid
     *
     * @return Channelgroup
     */
    public function setChannelgroupid($channelgroupid)
    {
        $this->channelgroupid = $channelgroupid;

        return $this;
    }

    /**
     * Get channelgroupid
     *
     * @return integer
     */
    public function getChannelgroupid()
    {
        return $this->channelgroupid;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Channelgroup
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}

