<?php

namespace Marketing\ModelBundle\Entity;

/**
 * Controllinggroup
 */
class Controllinggroup
{
    /**
     * @var integer
     */
    private $controllinggroupid;

    /**
     * @var string
     */
    private $name;


    /**
     * Set controllinggroupid
     *
     * @param integer $controllinggroupid
     *
     * @return Controllinggroup
     */
    public function setControllinggroupid($controllinggroupid)
    {
        $this->controllinggroupid = $controllinggroupid;

        return $this;
    }

    /**
     * Get controllinggroupid
     *
     * @return integer
     */
    public function getControllinggroupid()
    {
        return $this->controllinggroupid;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Controllinggroup
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}

