<?php

namespace Marketing\ModelBundle\Entity;

/**
 * Partnerpromogamelink
 */
class Partnerpromogamelink
{
    /**
     * @var integer
     */
    private $partnerid;

    /**
     * @var integer
     */
    private $gameid;

    /**
     * @var integer
     */
    private $supervisorpartnerid;

    /**
     * @var \DateTime
     */
    private $startdate = '0000-00-00';

    /**
     * @var \DateTime
     */
    private $enddate = '0000-00-00';

    /**
     * @var integer
     */
    private $partnerpromogamelinkid;

    /**
     * @var \DateTime
     */
    private $modifydate = 'CURRENT_TIMESTAMP';


    /**
     * Set partnerid
     *
     * @param integer $partnerid
     *
     * @return Partnerpromogamelink
     */
    public function setPartnerid($partnerid)
    {
        $this->partnerid = $partnerid;

        return $this;
    }

    /**
     * Get partnerid
     *
     * @return integer
     */
    public function getPartnerid()
    {
        return $this->partnerid;
    }

    /**
     * Set gameid
     *
     * @param integer $gameid
     *
     * @return Partnerpromogamelink
     */
    public function setGameid($gameid)
    {
        $this->gameid = $gameid;

        return $this;
    }

    /**
     * Get gameid
     *
     * @return integer
     */
    public function getGameid()
    {
        return $this->gameid;
    }

    /**
     * Set supervisorpartnerid
     *
     * @param integer $supervisorpartnerid
     *
     * @return Partnerpromogamelink
     */
    public function setSupervisorpartnerid($supervisorpartnerid)
    {
        $this->supervisorpartnerid = $supervisorpartnerid;

        return $this;
    }

    /**
     * Get supervisorpartnerid
     *
     * @return integer
     */
    public function getSupervisorpartnerid()
    {
        return $this->supervisorpartnerid;
    }

    /**
     * Set startdate
     *
     * @param \DateTime $startdate
     *
     * @return Partnerpromogamelink
     */
    public function setStartdate($startdate)
    {
        $this->startdate = $startdate;

        return $this;
    }

    /**
     * Get startdate
     *
     * @return \DateTime
     */
    public function getStartdate()
    {
        return $this->startdate;
    }

    /**
     * Set enddate
     *
     * @param \DateTime $enddate
     *
     * @return Partnerpromogamelink
     */
    public function setEnddate($enddate)
    {
        $this->enddate = $enddate;

        return $this;
    }

    /**
     * Get enddate
     *
     * @return \DateTime
     */
    public function getEnddate()
    {
        return $this->enddate;
    }

    /**
     * Set partnerpromogamelinkid
     *
     * @param integer $partnerpromogamelinkid
     *
     * @return Partnerpromogamelink
     */
    public function setPartnerpromogamelinkid($partnerpromogamelinkid)
    {
        $this->partnerpromogamelinkid = $partnerpromogamelinkid;

        return $this;
    }

    /**
     * Get partnerpromogamelinkid
     *
     * @return integer
     */
    public function getPartnerpromogamelinkid()
    {
        return $this->partnerpromogamelinkid;
    }

    /**
     * Set modifydate
     *
     * @param \DateTime $modifydate
     *
     * @return Partnerpromogamelink
     */
    public function setModifydate($modifydate)
    {
        $this->modifydate = $modifydate;

        return $this;
    }

    /**
     * Get modifydate
     *
     * @return \DateTime
     */
    public function getModifydate()
    {
        return $this->modifydate;
    }
}

