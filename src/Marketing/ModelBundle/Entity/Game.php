<?php

namespace Marketing\ModelBundle\Entity;

/**
 * Game
 */
class Game
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var integer
     */
    private $gameid;

    /**
     * @var string
     */
    private $name;

    /**
     * @var boolean
     */
    private $iscritical;

    /**
     * @var boolean
     */
    private $isactive = '1';

    /**
     * @var string
     */
    private $defaultcountry = 'ii';

    /**
     * @var \DateTime
     */
    private $startdate;

    /**
     * @var \DateTime
     */
    private $finishdate;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * Set gameid
     *
     * @param integer $gameid
     *
     * @return Game
     */
    public function setGameid($gameid)
    {
        $this->gameid = $gameid;

        return $this;
    }

    /**
     * Get gameid
     *
     * @return integer
     */
    public function getGameid()
    {
        return $this->gameid;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Game
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set iscritical
     *
     * @param boolean $iscritical
     *
     * @return Game
     */
    public function setIscritical($iscritical)
    {
        $this->iscritical = $iscritical;

        return $this;
    }

    /**
     * Get iscritical
     *
     * @return boolean
     */
    public function getIscritical()
    {
        return $this->iscritical;
    }

    /**
     * Set isactive
     *
     * @param boolean $isactive
     *
     * @return Game
     */
    public function setIsactive($isactive)
    {
        $this->isactive = $isactive;

        return $this;
    }

    /**
     * Get isactive
     *
     * @return boolean
     */
    public function getIsactive()
    {
        return $this->isactive;
    }

    /**
     * Set defaultcountry
     *
     * @param string $defaultcountry
     *
     * @return Game
     */
    public function setDefaultcountry($defaultcountry)
    {
        $this->defaultcountry = $defaultcountry;

        return $this;
    }

    /**
     * Get defaultcountry
     *
     * @return string
     */
    public function getDefaultcountry()
    {
        return $this->defaultcountry;
    }

    /**
     * Set startdate
     *
     * @param \DateTime $startdate
     *
     * @return Game
     */
    public function setStartdate($startdate)
    {
        $this->startdate = $startdate;

        return $this;
    }

    /**
     * Get startdate
     *
     * @return \DateTime
     */
    public function getStartdate()
    {
        return $this->startdate;
    }

    /**
     * Set finishdate
     *
     * @param \DateTime $finishdate
     *
     * @return Game
     */
    public function setFinishdate($finishdate)
    {
        $this->finishdate = $finishdate;

        return $this;
    }

    /**
     * Get finishdate
     *
     * @return \DateTime
     */
    public function getFinishdate()
    {
        return $this->finishdate;
    }
}

