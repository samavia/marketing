<?php

namespace Marketing\ModelBundle\Entity;

/**
 * Partner
 */
class Partner
{
    /**
     * @var integer
     */
    private $partnerid;

    /**
     * @var string
     */
    private $status = 'suspended';

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $creditnotename;

    /**
     * @var string
     */
    private $creditnoteemail;

    /**
     * @var integer
     */
    private $contactid;

    /**
     * @var integer
     */
    private $bankaccountid;

    /**
     * @var \DateTime
     */
    private $startdate = '2012-12-01';

    /**
     * @var \DateTime
     */
    private $finishdate;

    /**
     * @var string
     */
    private $reportinggroup;

    /**
     * @var integer
     */
    private $clicktagid = '1';

    /**
     * @var integer
     */
    private $creditornumber;


    /**
     * Set partnerid
     *
     * @param integer $partnerid
     *
     * @return Partner
     */
    public function setPartnerid($partnerid)
    {
        $this->partnerid = $partnerid;

        return $this;
    }

    /**
     * Get partnerid
     *
     * @return integer
     */
    public function getPartnerid()
    {
        return $this->partnerid;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return Partner
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Partner
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set creditnotename
     *
     * @param string $creditnotename
     *
     * @return Partner
     */
    public function setCreditnotename($creditnotename)
    {
        $this->creditnotename = $creditnotename;

        return $this;
    }

    /**
     * Get creditnotename
     *
     * @return string
     */
    public function getCreditnotename()
    {
        return $this->creditnotename;
    }

    /**
     * Set creditnoteemail
     *
     * @param string $creditnoteemail
     *
     * @return Partner
     */
    public function setCreditnoteemail($creditnoteemail)
    {
        $this->creditnoteemail = $creditnoteemail;

        return $this;
    }

    /**
     * Get creditnoteemail
     *
     * @return string
     */
    public function getCreditnoteemail()
    {
        return $this->creditnoteemail;
    }

    /**
     * Set contactid
     *
     * @param integer $contactid
     *
     * @return Partner
     */
    public function setContactid($contactid)
    {
        $this->contactid = $contactid;

        return $this;
    }

    /**
     * Get contactid
     *
     * @return integer
     */
    public function getContactid()
    {
        return $this->contactid;
    }

    /**
     * Set bankaccountid
     *
     * @param integer $bankaccountid
     *
     * @return Partner
     */
    public function setBankaccountid($bankaccountid)
    {
        $this->bankaccountid = $bankaccountid;

        return $this;
    }

    /**
     * Get bankaccountid
     *
     * @return integer
     */
    public function getBankaccountid()
    {
        return $this->bankaccountid;
    }

    /**
     * Set startdate
     *
     * @param \DateTime $startdate
     *
     * @return Partner
     */
    public function setStartdate($startdate)
    {
        $this->startdate = $startdate;

        return $this;
    }

    /**
     * Get startdate
     *
     * @return \DateTime
     */
    public function getStartdate()
    {
        return $this->startdate;
    }

    /**
     * Set finishdate
     *
     * @param \DateTime $finishdate
     *
     * @return Partner
     */
    public function setFinishdate($finishdate)
    {
        $this->finishdate = $finishdate;

        return $this;
    }

    /**
     * Get finishdate
     *
     * @return \DateTime
     */
    public function getFinishdate()
    {
        return $this->finishdate;
    }

    /**
     * Set reportinggroup
     *
     * @param string $reportinggroup
     *
     * @return Partner
     */
    public function setReportinggroup($reportinggroup)
    {
        $this->reportinggroup = $reportinggroup;

        return $this;
    }

    /**
     * Get reportinggroup
     *
     * @return string
     */
    public function getReportinggroup()
    {
        return $this->reportinggroup;
    }

    /**
     * Set clicktagid
     *
     * @param integer $clicktagid
     *
     * @return Partner
     */
    public function setClicktagid($clicktagid)
    {
        $this->clicktagid = $clicktagid;

        return $this;
    }

    /**
     * Get clicktagid
     *
     * @return integer
     */
    public function getClicktagid()
    {
        return $this->clicktagid;
    }

    /**
     * Set creditornumber
     *
     * @param integer $creditornumber
     *
     * @return Partner
     */
    public function setCreditornumber($creditornumber)
    {
        $this->creditornumber = $creditornumber;

        return $this;
    }

    /**
     * Get creditornumber
     *
     * @return integer
     */
    public function getCreditornumber()
    {
        return $this->creditornumber;
    }
}

