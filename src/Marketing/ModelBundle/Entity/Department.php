<?php

namespace Marketing\ModelBundle\Entity;

/**
 * Department
 */
class Department
{
    /**
     * @var integer
     */
    private $departmentid;

    /**
     * @var string
     */
    private $name;


    /**
     * Set departmentid
     *
     * @param integer $departmentid
     *
     * @return Department
     */
    public function setDepartmentid($departmentid)
    {
        $this->departmentid = $departmentid;

        return $this;
    }

    /**
     * Get departmentid
     *
     * @return integer
     */
    public function getDepartmentid()
    {
        return $this->departmentid;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Department
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}

