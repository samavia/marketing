<?php

namespace Marketing\ModelBundle\Entity;

/**
 * Useradcodelink
 */
class Useradcodelink
{
    /**
     * @var integer
     */
    private $userid;

    /**
     * @var integer
     */
    private $adcodeid;

    /**
     * @var integer
     */
    private $roleid;


    /**
     * Set userid
     *
     * @param integer $userid
     *
     * @return Useradcodelink
     */
    public function setUserid($userid)
    {
        $this->userid = $userid;

        return $this;
    }

    /**
     * Get userid
     *
     * @return integer
     */
    public function getUserid()
    {
        return $this->userid;
    }

    /**
     * Set adcodeid
     *
     * @param integer $adcodeid
     *
     * @return Useradcodelink
     */
    public function setAdcodeid($adcodeid)
    {
        $this->adcodeid = $adcodeid;

        return $this;
    }

    /**
     * Get adcodeid
     *
     * @return integer
     */
    public function getAdcodeid()
    {
        return $this->adcodeid;
    }

    /**
     * Set roleid
     *
     * @param integer $roleid
     *
     * @return Useradcodelink
     */
    public function setRoleid($roleid)
    {
        $this->roleid = $roleid;

        return $this;
    }

    /**
     * Get roleid
     *
     * @return integer
     */
    public function getRoleid()
    {
        return $this->roleid;
    }
}

