<?php

namespace Marketing\ModelBundle\Entity;

/**
 * Paymentmodel
 */
class Paymentmodel
{
    /**
     * @var integer
     */
    private $paymentmodelid;

    /**
     * @var string
     */
    private $name;


    /**
     * Set paymentmodelid
     *
     * @param integer $paymentmodelid
     *
     * @return Paymentmodel
     */
    public function setPaymentmodelid($paymentmodelid)
    {
        $this->paymentmodelid = $paymentmodelid;

        return $this;
    }

    /**
     * Get paymentmodelid
     *
     * @return integer
     */
    public function getPaymentmodelid()
    {
        return $this->paymentmodelid;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Paymentmodel
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}

