<?php

namespace Marketing\ModelBundle\Entity;

/**
 * Clicktag
 */
class Clicktag
{
    /**
     * @var integer
     */
    private $clicktagid;

    /**
     * @var string
     */
    private $name;


    /**
     * Set clicktagid
     *
     * @param integer $clicktagid
     *
     * @return Clicktag
     */
    public function setClicktagid($clicktagid)
    {
        $this->clicktagid = $clicktagid;

        return $this;
    }

    /**
     * Get clicktagid
     *
     * @return integer
     */
    public function getClicktagid()
    {
        return $this->clicktagid;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Clicktag
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}

