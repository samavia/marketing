<?php

namespace Marketing\ModelBundle\Entity;

/**
 * Country
 */
class Country
{
    /**
     * @var string
     */
    private $countryid;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $iso3code;

    /**
     * @var string
     */
    private $digitcode;

    /**
     * @var boolean
     */
    private $mediacode;

    /**
     * @var string
     */
    private $region;


    /**
     * Set countryid
     *
     * @param string $countryid
     *
     * @return Country
     */
    public function setCountryid($countryid)
    {
        $this->countryid = $countryid;

        return $this;
    }

    /**
     * Get countryid
     *
     * @return string
     */
    public function getCountryid()
    {
        return $this->countryid;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Country
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set iso3code
     *
     * @param string $iso3code
     *
     * @return Country
     */
    public function setIso3code($iso3code)
    {
        $this->iso3code = $iso3code;

        return $this;
    }

    /**
     * Get iso3code
     *
     * @return string
     */
    public function getIso3code()
    {
        return $this->iso3code;
    }

    /**
     * Set digitcode
     *
     * @param string $digitcode
     *
     * @return Country
     */
    public function setDigitcode($digitcode)
    {
        $this->digitcode = $digitcode;

        return $this;
    }

    /**
     * Get digitcode
     *
     * @return string
     */
    public function getDigitcode()
    {
        return $this->digitcode;
    }

    /**
     * Set mediacode
     *
     * @param boolean $mediacode
     *
     * @return Country
     */
    public function setMediacode($mediacode)
    {
        $this->mediacode = $mediacode;

        return $this;
    }

    /**
     * Get mediacode
     *
     * @return boolean
     */
    public function getMediacode()
    {
        return $this->mediacode;
    }

    /**
     * Set region
     *
     * @param string $region
     *
     * @return Country
     */
    public function setRegion($region)
    {
        $this->region = $region;

        return $this;
    }

    /**
     * Get region
     *
     * @return string
     */
    public function getRegion()
    {
        return $this->region;
    }
}

