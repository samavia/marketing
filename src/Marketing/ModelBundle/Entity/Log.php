<?php

namespace Marketing\ModelBundle\Entity;

/**
 * Log
 */
class Log
{
    /**
     * @var integer
     */
    private $logid;

    /**
     * @var \DateTime
     */
    private $logtime;

    /**
     * @var integer
     */
    private $userid;

    /**
     * @var string
     */
    private $comment;


    /**
     * Set logid
     *
     * @param integer $logid
     *
     * @return Log
     */
    public function setLogid($logid)
    {
        $this->logid = $logid;

        return $this;
    }

    /**
     * Get logid
     *
     * @return integer
     */
    public function getLogid()
    {
        return $this->logid;
    }

    /**
     * Set logtime
     *
     * @param \DateTime $logtime
     *
     * @return Log
     */
    public function setLogtime($logtime)
    {
        $this->logtime = $logtime;

        return $this;
    }

    /**
     * Get logtime
     *
     * @return \DateTime
     */
    public function getLogtime()
    {
        return $this->logtime;
    }

    /**
     * Set userid
     *
     * @param integer $userid
     *
     * @return Log
     */
    public function setUserid($userid)
    {
        $this->userid = $userid;

        return $this;
    }

    /**
     * Get userid
     *
     * @return integer
     */
    public function getUserid()
    {
        return $this->userid;
    }

    /**
     * Set comment
     *
     * @param string $comment
     *
     * @return Log
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }
}

