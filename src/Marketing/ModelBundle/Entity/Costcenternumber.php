<?php

namespace Marketing\ModelBundle\Entity;

/**
 * Costcenternumber
 */
class Costcenternumber
{
    /**
     * @var integer
     */
    private $costcenternumberid;

    /**
     * @var string
     */
    private $costcenternumber;

    /**
     * @var integer
     */
    private $gameid;

    /**
     * @var integer
     */
    private $paymentmodelid;


    /**
     * Set costcenternumberid
     *
     * @param integer $costcenternumberid
     *
     * @return Costcenternumber
     */
    public function setCostcenternumberid($costcenternumberid)
    {
        $this->costcenternumberid = $costcenternumberid;

        return $this;
    }

    /**
     * Get costcenternumberid
     *
     * @return integer
     */
    public function getCostcenternumberid()
    {
        return $this->costcenternumberid;
    }

    /**
     * Set costcenternumber
     *
     * @param string $costcenternumber
     *
     * @return Costcenternumber
     */
    public function setCostcenternumber($costcenternumber)
    {
        $this->costcenternumber = $costcenternumber;

        return $this;
    }

    /**
     * Get costcenternumber
     *
     * @return string
     */
    public function getCostcenternumber()
    {
        return $this->costcenternumber;
    }

    /**
     * Set gameid
     *
     * @param integer $gameid
     *
     * @return Costcenternumber
     */
    public function setGameid($gameid)
    {
        $this->gameid = $gameid;

        return $this;
    }

    /**
     * Get gameid
     *
     * @return integer
     */
    public function getGameid()
    {
        return $this->gameid;
    }

    /**
     * Set paymentmodelid
     *
     * @param integer $paymentmodelid
     *
     * @return Costcenternumber
     */
    public function setPaymentmodelid($paymentmodelid)
    {
        $this->paymentmodelid = $paymentmodelid;

        return $this;
    }

    /**
     * Get paymentmodelid
     *
     * @return integer
     */
    public function getPaymentmodelid()
    {
        return $this->paymentmodelid;
    }
}

