<?php

namespace Marketing\ModelBundle\Entity;

/**
 * Databasechangelog
 */
class Databasechangelog
{
    /**
     * @var string
     */
    private $id;

    /**
     * @var string
     */
    private $author;

    /**
     * @var string
     */
    private $filename;

    /**
     * @var \DateTime
     */
    private $dateexecuted;

    /**
     * @var integer
     */
    private $orderexecuted;

    /**
     * @var string
     */
    private $exectype;

    /**
     * @var string
     */
    private $md5sum;

    /**
     * @var string
     */
    private $description;

    /**
     * @var string
     */
    private $comments;

    /**
     * @var string
     */
    private $tag;

    /**
     * @var string
     */
    private $liquibase;


    /**
     * Set id
     *
     * @param string $id
     *
     * @return Databasechangelog
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set author
     *
     * @param string $author
     *
     * @return Databasechangelog
     */
    public function setAuthor($author)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get author
     *
     * @return string
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * Set filename
     *
     * @param string $filename
     *
     * @return Databasechangelog
     */
    public function setFilename($filename)
    {
        $this->filename = $filename;

        return $this;
    }

    /**
     * Get filename
     *
     * @return string
     */
    public function getFilename()
    {
        return $this->filename;
    }

    /**
     * Set dateexecuted
     *
     * @param \DateTime $dateexecuted
     *
     * @return Databasechangelog
     */
    public function setDateexecuted($dateexecuted)
    {
        $this->dateexecuted = $dateexecuted;

        return $this;
    }

    /**
     * Get dateexecuted
     *
     * @return \DateTime
     */
    public function getDateexecuted()
    {
        return $this->dateexecuted;
    }

    /**
     * Set orderexecuted
     *
     * @param integer $orderexecuted
     *
     * @return Databasechangelog
     */
    public function setOrderexecuted($orderexecuted)
    {
        $this->orderexecuted = $orderexecuted;

        return $this;
    }

    /**
     * Get orderexecuted
     *
     * @return integer
     */
    public function getOrderexecuted()
    {
        return $this->orderexecuted;
    }

    /**
     * Set exectype
     *
     * @param string $exectype
     *
     * @return Databasechangelog
     */
    public function setExectype($exectype)
    {
        $this->exectype = $exectype;

        return $this;
    }

    /**
     * Get exectype
     *
     * @return string
     */
    public function getExectype()
    {
        return $this->exectype;
    }

    /**
     * Set md5sum
     *
     * @param string $md5sum
     *
     * @return Databasechangelog
     */
    public function setMd5sum($md5sum)
    {
        $this->md5sum = $md5sum;

        return $this;
    }

    /**
     * Get md5sum
     *
     * @return string
     */
    public function getMd5sum()
    {
        return $this->md5sum;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Databasechangelog
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set comments
     *
     * @param string $comments
     *
     * @return Databasechangelog
     */
    public function setComments($comments)
    {
        $this->comments = $comments;

        return $this;
    }

    /**
     * Get comments
     *
     * @return string
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * Set tag
     *
     * @param string $tag
     *
     * @return Databasechangelog
     */
    public function setTag($tag)
    {
        $this->tag = $tag;

        return $this;
    }

    /**
     * Get tag
     *
     * @return string
     */
    public function getTag()
    {
        return $this->tag;
    }

    /**
     * Set liquibase
     *
     * @param string $liquibase
     *
     * @return Databasechangelog
     */
    public function setLiquibase($liquibase)
    {
        $this->liquibase = $liquibase;

        return $this;
    }

    /**
     * Get liquibase
     *
     * @return string
     */
    public function getLiquibase()
    {
        return $this->liquibase;
    }
}

