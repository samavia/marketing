<?php

namespace Marketing\ModelBundle\Entity;

/**
 * Channel
 */
class Channel
{
    /**
     * @var integer
     */
    private $channelid;

    /**
     * @var string
     */
    private $name;

    /**
     * @var integer
     */
    private $departmentid;


    /**
     * Set channelid
     *
     * @param integer $channelid
     *
     * @return Channel
     */
    public function setChannelid($channelid)
    {
        $this->channelid = $channelid;

        return $this;
    }

    /**
     * Get channelid
     *
     * @return integer
     */
    public function getChannelid()
    {
        return $this->channelid;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Channel
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set departmentid
     *
     * @param integer $departmentid
     *
     * @return Channel
     */
    public function setDepartmentid($departmentid)
    {
        $this->departmentid = $departmentid;

        return $this;
    }

    /**
     * Get departmentid
     *
     * @return integer
     */
    public function getDepartmentid()
    {
        return $this->departmentid;
    }
}

