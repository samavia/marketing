<?php

namespace Marketing\ModelBundle\Entity;

/**
 * Adcode
 */
class Adcode
{
    /**
     * @var integer
     */
    private $adcodeid;

    /**
     * @var string
     */
    private $status = 'active';

    /**
     * @var string
     */
    private $name;

    /**
     * @var integer
     */
    private $channelid;

    /**
     * @var integer
     */
    private $partnerid;

    /**
     * @var integer
     */
    private $paymentmodelid;

    /**
     * @var string
     */
    private $description;


    /**
     * Set adcodeid
     *
     * @param integer $adcodeid
     *
     * @return Adcode
     */
    public function setAdcodeid($adcodeid)
    {
        $this->adcodeid = $adcodeid;

        return $this;
    }

    /**
     * Get adcodeid
     *
     * @return integer
     */
    public function getAdcodeid()
    {
        return $this->adcodeid;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return Adcode
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Adcode
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set channelid
     *
     * @param integer $channelid
     *
     * @return Adcode
     */
    public function setChannelid($channelid)
    {
        $this->channelid = $channelid;

        return $this;
    }

    /**
     * Get channelid
     *
     * @return integer
     */
    public function getChannelid()
    {
        return $this->channelid;
    }

    /**
     * Set partnerid
     *
     * @param integer $partnerid
     *
     * @return Adcode
     */
    public function setPartnerid($partnerid)
    {
        $this->partnerid = $partnerid;

        return $this;
    }

    /**
     * Get partnerid
     *
     * @return integer
     */
    public function getPartnerid()
    {
        return $this->partnerid;
    }

    /**
     * Set paymentmodelid
     *
     * @param integer $paymentmodelid
     *
     * @return Adcode
     */
    public function setPaymentmodelid($paymentmodelid)
    {
        $this->paymentmodelid = $paymentmodelid;

        return $this;
    }

    /**
     * Get paymentmodelid
     *
     * @return integer
     */
    public function getPaymentmodelid()
    {
        return $this->paymentmodelid;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Adcode
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }
}

