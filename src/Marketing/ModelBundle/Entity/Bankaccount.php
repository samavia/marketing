<?php

namespace Marketing\ModelBundle\Entity;

/**
 * Bankaccount
 */
class Bankaccount
{
    /**
     * @var integer
     */
    private $bankaccountid;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $bankname;

    /**
     * @var string
     */
    private $number;

    /**
     * @var string
     */
    private $iban;

    /**
     * @var string
     */
    private $blz;

    /**
     * @var string
     */
    private $swift;

    /**
     * @var string
     */
    private $vatnumber;

    /**
     * @var string
     */
    private $currencyid;

    /**
     * @var string
     */
    private $type = 'bank';


    /**
     * Set bankaccountid
     *
     * @param integer $bankaccountid
     *
     * @return Bankaccount
     */
    public function setBankaccountid($bankaccountid)
    {
        $this->bankaccountid = $bankaccountid;

        return $this;
    }

    /**
     * Get bankaccountid
     *
     * @return integer
     */
    public function getBankaccountid()
    {
        return $this->bankaccountid;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Bankaccount
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set bankname
     *
     * @param string $bankname
     *
     * @return Bankaccount
     */
    public function setBankname($bankname)
    {
        $this->bankname = $bankname;

        return $this;
    }

    /**
     * Get bankname
     *
     * @return string
     */
    public function getBankname()
    {
        return $this->bankname;
    }

    /**
     * Set number
     *
     * @param string $number
     *
     * @return Bankaccount
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get number
     *
     * @return string
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set iban
     *
     * @param string $iban
     *
     * @return Bankaccount
     */
    public function setIban($iban)
    {
        $this->iban = $iban;

        return $this;
    }

    /**
     * Get iban
     *
     * @return string
     */
    public function getIban()
    {
        return $this->iban;
    }

    /**
     * Set blz
     *
     * @param string $blz
     *
     * @return Bankaccount
     */
    public function setBlz($blz)
    {
        $this->blz = $blz;

        return $this;
    }

    /**
     * Get blz
     *
     * @return string
     */
    public function getBlz()
    {
        return $this->blz;
    }

    /**
     * Set swift
     *
     * @param string $swift
     *
     * @return Bankaccount
     */
    public function setSwift($swift)
    {
        $this->swift = $swift;

        return $this;
    }

    /**
     * Get swift
     *
     * @return string
     */
    public function getSwift()
    {
        return $this->swift;
    }

    /**
     * Set vatnumber
     *
     * @param string $vatnumber
     *
     * @return Bankaccount
     */
    public function setVatnumber($vatnumber)
    {
        $this->vatnumber = $vatnumber;

        return $this;
    }

    /**
     * Get vatnumber
     *
     * @return string
     */
    public function getVatnumber()
    {
        return $this->vatnumber;
    }

    /**
     * Set currencyid
     *
     * @param string $currencyid
     *
     * @return Bankaccount
     */
    public function setCurrencyid($currencyid)
    {
        $this->currencyid = $currencyid;

        return $this;
    }

    /**
     * Get currencyid
     *
     * @return string
     */
    public function getCurrencyid()
    {
        return $this->currencyid;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Bankaccount
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }
}

