<?php

namespace Marketing\ModelBundle\Entity;

/**
 * Grouptochannellink
 */
class Grouptochannellink
{
    /**
     * @var integer
     */
    private $channelgroupid;

    /**
     * @var integer
     */
    private $channelid;

    /**
     * @var integer
     */
    private $controllinggroupid;


    /**
     * Set channelgroupid
     *
     * @param integer $channelgroupid
     *
     * @return Grouptochannellink
     */
    public function setChannelgroupid($channelgroupid)
    {
        $this->channelgroupid = $channelgroupid;

        return $this;
    }

    /**
     * Get channelgroupid
     *
     * @return integer
     */
    public function getChannelgroupid()
    {
        return $this->channelgroupid;
    }

    /**
     * Set channelid
     *
     * @param integer $channelid
     *
     * @return Grouptochannellink
     */
    public function setChannelid($channelid)
    {
        $this->channelid = $channelid;

        return $this;
    }

    /**
     * Get channelid
     *
     * @return integer
     */
    public function getChannelid()
    {
        return $this->channelid;
    }

    /**
     * Set controllinggroupid
     *
     * @param integer $controllinggroupid
     *
     * @return Grouptochannellink
     */
    public function setControllinggroupid($controllinggroupid)
    {
        $this->controllinggroupid = $controllinggroupid;

        return $this;
    }

    /**
     * Get controllinggroupid
     *
     * @return integer
     */
    public function getControllinggroupid()
    {
        return $this->controllinggroupid;
    }
}

