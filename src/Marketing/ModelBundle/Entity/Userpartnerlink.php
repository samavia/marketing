<?php

namespace Marketing\ModelBundle\Entity;

/**
 * Userpartnerlink
 */
class Userpartnerlink
{
    /**
     * @var integer
     */
    private $userid;

    /**
     * @var integer
     */
    private $partnerid;

    /**
     * @var integer
     */
    private $roleid;

    /**
     * @var boolean
     */
    private $newsletters = '0';


    /**
     * Set userid
     *
     * @param integer $userid
     *
     * @return Userpartnerlink
     */
    public function setUserid($userid)
    {
        $this->userid = $userid;

        return $this;
    }

    /**
     * Get userid
     *
     * @return integer
     */
    public function getUserid()
    {
        return $this->userid;
    }

    /**
     * Set partnerid
     *
     * @param integer $partnerid
     *
     * @return Userpartnerlink
     */
    public function setPartnerid($partnerid)
    {
        $this->partnerid = $partnerid;

        return $this;
    }

    /**
     * Get partnerid
     *
     * @return integer
     */
    public function getPartnerid()
    {
        return $this->partnerid;
    }

    /**
     * Set roleid
     *
     * @param integer $roleid
     *
     * @return Userpartnerlink
     */
    public function setRoleid($roleid)
    {
        $this->roleid = $roleid;

        return $this;
    }

    /**
     * Get roleid
     *
     * @return integer
     */
    public function getRoleid()
    {
        return $this->roleid;
    }

    /**
     * Set newsletters
     *
     * @param boolean $newsletters
     *
     * @return Userpartnerlink
     */
    public function setNewsletters($newsletters)
    {
        $this->newsletters = $newsletters;

        return $this;
    }

    /**
     * Get newsletters
     *
     * @return boolean
     */
    public function getNewsletters()
    {
        return $this->newsletters;
    }
}

