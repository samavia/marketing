<?php

namespace Marketing\ModelBundle\Entity;

/**
 * Databasechangeloglock
 */
class Databasechangeloglock
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var boolean
     */
    private $locked;

    /**
     * @var \DateTime
     */
    private $lockgranted;

    /**
     * @var string
     */
    private $lockedby;


    /**
     * Set id
     *
     * @param integer $id
     *
     * @return Databasechangeloglock
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set locked
     *
     * @param boolean $locked
     *
     * @return Databasechangeloglock
     */
    public function setLocked($locked)
    {
        $this->locked = $locked;

        return $this;
    }

    /**
     * Get locked
     *
     * @return boolean
     */
    public function getLocked()
    {
        return $this->locked;
    }

    /**
     * Set lockgranted
     *
     * @param \DateTime $lockgranted
     *
     * @return Databasechangeloglock
     */
    public function setLockgranted($lockgranted)
    {
        $this->lockgranted = $lockgranted;

        return $this;
    }

    /**
     * Get lockgranted
     *
     * @return \DateTime
     */
    public function getLockgranted()
    {
        return $this->lockgranted;
    }

    /**
     * Set lockedby
     *
     * @param string $lockedby
     *
     * @return Databasechangeloglock
     */
    public function setLockedby($lockedby)
    {
        $this->lockedby = $lockedby;

        return $this;
    }

    /**
     * Get lockedby
     *
     * @return string
     */
    public function getLockedby()
    {
        return $this->lockedby;
    }
}

